﻿namespace Base2art.Collections
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using FluentAssertions;
    using Xunit;

    public class EnumerableExtenderFeatures
    {
        [Fact]
        public void ShouldDoForAll()
        {
            var coll = new[] {"a", "b", "c"};
            var i = 0;
            var cat = string.Empty;
            coll.ForAll(x => i += 1);
            coll.ForAll(x => cat += x);
            i.Should().Be(3);
            cat.Should().Be("abc");
        }

        [Fact]
        public void ShouldDoIndexOfArray()
        {
            "a".In((IEnumerable<string>) new[] {"a", "b", "c"}).Should().BeTrue();
            "d".In((IEnumerable<string>) new[] {"a", "b", "c"}).Should().BeFalse();

            "a".In("a", "b", "c").Should().BeTrue();
            "d".In("a", "b", "c").Should().BeFalse();
        }

        [Fact]
        public void ShouldDoIndexOfEnumerable()
        {
            var coll = new[] {"a", "b", "c"};
            coll.IndexOf("a").Should().Be(0);
            coll.IndexOf("b").Should().Be(1);
            coll.IndexOf("c").Should().Be(2);
            new Action(() => coll.IndexOf("d")).ShouldThrow<KeyNotFoundException>();
        }

        [Fact]
        public void ShouldGetArrayEnumerator()
        {
            var coll = new[] {"a", "b", "c"};
            var c1 = coll.AsEnumerable();

            c1.Should().Equal(coll);
        }

        [Fact]
        public void ShouldGetGenericEnumerator()
        {
            var coll = new[] {"a", "b", "c"};
            var list = new List<string>();
            var c1 = coll.GetGenericEnumerator();
            while (c1.MoveNext())
            {
                list.Add(c1.Current);
            }
        }

        [Fact]
        public void ShouldPartition_superSimple()
        {
            var ints = new List<int> {0};
            var data = ints.PartitionByMaxGroupSize(4);
            data.Count().Should().Be(1);
        }

        [Fact]
        public void ShouldPartition_superSimple2()
        {
            var ints = new List<int> {0, 1};
            var data = ints.PartitionByMaxGroupSize(1);
            var enumerator = data.GetEnumerator();

            enumerator.MoveNext().Should().BeTrue();
            var se1 = enumerator.Current.GetEnumerator();
            se1.MoveNext().Should().BeTrue();
            se1.Current.Should().Be(0);
            se1.MoveNext().Should().BeFalse();
            enumerator.MoveNext().Should().BeTrue();
            var se2 = enumerator.Current.GetEnumerator();
            se2.MoveNext().Should().BeTrue();
            se2.Current.Should().Be(1);
            se2.MoveNext().Should().BeFalse();
            enumerator.MoveNext().Should().BeFalse();
        }

        [Fact]
        public void ShouldPartition_PartialEnumeration()
        {
            var ints = new List<int> {0, 1, 2, 3, 4};
            var data = ints.PartitionByMaxGroupSize(2);
            var enumerator = data.GetEnumerator();

            enumerator.MoveNext().Should().BeTrue();
            
            var se1 = enumerator.Current.GetEnumerator();
            se1.MoveNext().Should().BeTrue();
            se1.Current.Should().Be(0);
            se1.MoveNext().Should().BeTrue();
//            se1.Current.Should().Be(1);
//            se1.MoveNext().Should().BeFalse();
            
            enumerator.MoveNext().Should().BeTrue();
            
            var se2 = enumerator.Current.GetEnumerator();
            se2.MoveNext().Should().BeTrue();
            se2.Current.Should().Be(2);
            se2.MoveNext().Should().BeTrue();
            se2.Current.Should().Be(3);
            se2.MoveNext().Should().BeFalse();
            
            enumerator.MoveNext().Should().BeTrue();
            
            var se3 = enumerator.Current.GetEnumerator();
            se3.MoveNext().Should().BeTrue();
            se3.Current.Should().Be(4);
            se3.MoveNext().Should().BeFalse();
            
            enumerator.MoveNext().Should().BeFalse();
        }

        [Fact]
        public void ShouldPartition_superSimple1()
        {
            var ints = new List<int> {0};
            var data = ints.PartitionByMaxGroupSize(4);
            data.Count().Should().Be(1);
            data.Skip(0).First().Count().Should().Be(1);
            data.Skip(0).First().ToList()[0].Should().Be(0);
        }

        [Fact]
        public void ShouldPartition()
        {
            var ints = new List<int> {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
            var data = ints.PartitionByMaxGroupSize(3);
            data.Count().Should().Be(4);

            data.Skip(0).First().Count().Should().Be(3);
            data.Skip(0).First().ToList()[0].Should().Be(0);
            data.Skip(0).First().ToList()[1].Should().Be(1);
            data.Skip(0).First().ToList()[2].Should().Be(2);

            data.Skip(1).First().Count().Should().Be(3);
            data.Skip(1).First().ToList()[0].Should().Be(3);
            data.Skip(1).First().ToList()[1].Should().Be(4);
            data.Skip(1).First().ToList()[2].Should().Be(5);

            data.Skip(2).First().Count().Should().Be(3);
            data.Skip(2).First().ToList()[0].Should().Be(6);
            data.Skip(2).First().ToList()[1].Should().Be(7);
            data.Skip(2).First().ToList()[2].Should().Be(8);

            data.Skip(3).First().Count().Should().Be(1);
            data.Skip(3).First().ToList()[0].Should().Be(9);
        }

        [Fact]
        public void ShouldPartition_Even()
        {
            var ints = new List<int> {0, 1, 2, 3, 4, 5, 6, 7, 8};
            var data = ints.PartitionByMaxGroupSize(3);
            data.Count().Should().Be(3);

            data.Skip(0).First().Count().Should().Be(3);
            data.Skip(0).First().ToList()[0].Should().Be(0);
            data.Skip(0).First().ToList()[1].Should().Be(1);
            data.Skip(0).First().ToList()[2].Should().Be(2);

            data.Skip(1).First().Count().Should().Be(3);
            data.Skip(1).First().ToList()[0].Should().Be(3);
            data.Skip(1).First().ToList()[1].Should().Be(4);
            data.Skip(1).First().ToList()[2].Should().Be(5);

            data.Skip(2).First().Count().Should().Be(3);
            data.Skip(2).First().ToList()[0].Should().Be(6);
            data.Skip(2).First().ToList()[1].Should().Be(7);
            data.Skip(2).First().ToList()[2].Should().Be(8);
        }

        [Fact]
        public void ShouldPartition_Even_Fix()
        {
            var ints = new List<int> {0, 1, 2, 3, 4, 5, 6, 7, 8};
            var data = ints.PartitionByMaxGroupSize(3);
            data.Count().Should().Be(3);

            data.Skip(0).First().Count().Should().Be(3);
            data.Skip(0).First().ToList()[0].Should().Be(0);
            data.Skip(0).First().ToList()[1].Should().Be(1);
            data.Skip(0).First().ToList()[2].Should().Be(2);

            data.Skip(1).First().Count().Should().Be(3);
            data.Skip(1).First().ToList()[0].Should().Be(3);
            data.Skip(1).First().ToList()[1].Should().Be(4);
            data.Skip(1).First().ToList()[2].Should().Be(5);

            data.Skip(2).First().Count().Should().Be(3);
            data.Skip(2).First().ToList()[0].Should().Be(6);
            data.Skip(2).First().ToList()[1].Should().Be(7);
            data.Skip(2).First().ToList()[2].Should().Be(8);

//            data.Skip(3).First().Count().Should().Be(0);
        }
    }
}