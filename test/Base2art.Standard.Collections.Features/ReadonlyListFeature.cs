﻿namespace Base2art.Collections
{
    using System.Collections.Generic;
    using System.Linq;
    using FluentAssertions;
    using Xunit;

    public class ReadonlyListFeature
    {
        [Fact]
        public void ShouldHaveArrayApi()
        {
            var arr = new[] {"a", "b"}.AsReadOnlyArrayList();
            var item = arr[1];
            item.Should().Be("b");
            arr.Count.Should().Be(2);

            arr.ToList().Count.Should().Be(2);
            arr.OfType<string>().ToList().Count.Should().Be(2);
        }

        [Fact]
        public void ShouldHaveListApi()
        {
            var values = new List<string> {"a", "b"};
            var arr = values.AsReadOnlyArrayList();
            var item = arr[1];
            item.Should().Be("b");
            arr.Count.Should().Be(2);

            arr.ToList().Count.Should().Be(2);
            arr.OfType<string>().ToList().Count.Should().Be(2);
        }
    }
}