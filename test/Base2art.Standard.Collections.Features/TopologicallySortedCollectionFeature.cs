﻿namespace Base2art.Collections
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using FluentAssertions;
    using Xunit;

    public class TopologicallySortedCollectionFeature
    {
        private TopologicallySortedCollection<string, Person> CreateStandardCollection()
        {
            var collection = new DelegatedTopologicallySortedCollection<string, Person>(
                                                                                        x => x.Name,
                                                                                        x => x.Requires);
            collection.IsReadOnly.Should().BeFalse();

            collection.Add(new Person {Name = "A"});
            collection.Add(new Person {Name = "E", Requires = new[] {"D"}});
            collection.Add(new Person {Name = "B", Requires = new[] {"A"}});
            collection.Add(new Person {Name = "D", Requires = new[] {"C"}});
            collection.Add(new Person {Name = "C", Requires = new[] {"B"}});

            var items = collection.ToArray();
            items[0].Name.Should().Be("A");
            items[1].Name.Should().Be("B");
            items[2].Name.Should().Be("C");
            items[3].Name.Should().Be("D");
            items[4].Name.Should().Be("E");

            return collection;
        }

        private class Person
        {
            public string Name { get; set; }
            public string[] Requires { get; set; }
        }

        [Fact]
        public void ShoudEnumerate()
        {
            var collection = this.CreateStandardCollection();
            var people = new List<Person>();
            var people1 = new List<Person>();

            for (var i = ((IEnumerable) collection).GetEnumerator(); i.MoveNext();)
            {
                var element = i.Current;
                people1.Add((Person) element);
            }

            collection.ForAll(people.Add);
            var peopleA1 = people.ToArray();
            var peopleA2 = people.ToArray();
            var resolved = collection.Resolve();
            peopleA1.Length.Should().Be(5);
            peopleA2.Length.Should().Be(5);
            resolved.Length.Should().Be(5);
            for (var i = 0; i < 5; i++)
            {
                peopleA1[i].Name.Should().Be(resolved[i].Name);
                peopleA1[i].Name.Should().Be(peopleA2[i].Name);
                collection.Skip(i).First().Name.Should().Be(resolved[i].Name);
            }
        }

        [Fact]
        public void ShouldCheckParams()
        {
            new Action(() => new DelegatedTopologicallySortedCollection<string, Person>(null, x => x.Requires))
                .ShouldThrow<ArgumentNullException>();
            new Action(() => new DelegatedTopologicallySortedCollection<string, Person>(x => x.Name, null))
                .ShouldThrow<ArgumentNullException>();
        }

        [Fact]
        public void ShouldClear()
        {
            var collection = this.CreateStandardCollection();
            collection.Clear();

            var items = collection.ToArray();
            items.Length.Should().Be(0);
        }

        [Fact]
        public void ShouldRecognizeCircularDependency()
        {
            var collection = new DelegatedTopologicallySortedCollection<string, Person>(
                                                                                        x => x.Name,
                                                                                        x => x.Requires);

            collection.Add(new Person {Name = "A", Requires = new[] {"D"}});
            collection.Add(new Person {Name = "B", Requires = new[] {"C"}});
            collection.Add(new Person {Name = "C", Requires = new[] {"D"}});

            new Action(() =>
                           collection.Add(new Person
                                          {
                                              Name = "D",
                                              Requires = new[] {"A"}
                                          }))
                .ShouldThrow<InvalidOperationException>();
        }

        [Fact]
        public void ShouldRemove()
        {
            var collection = this.CreateStandardCollection();

            collection.Contains(new Person {Name = "A"}).Should().BeTrue();
            collection.Contains(new Person {Name = "B"}).Should().BeTrue();
            collection.Contains(new Person {Name = "F"}).Should().BeFalse();

            collection.Remove(new Person
                              {
                                  Name = "B",
                                  Requires = new[] {"A"}
                              }).Should().BeTrue();

            var items = collection.ToArray();
            items[0].Name.Should().Be("C");
            items[1].Name.Should().Be("D");
            items[2].Name.Should().Be("E");
            items[3].Name.Should().Be("A");

            collection.Contains(new Person {Name = "A"}).Should().BeTrue();
            collection.Contains(new Person {Name = "B"}).Should().BeFalse();
            collection.Contains(new Person {Name = "F"}).Should().BeFalse();

            collection.Remove(new Person
                              {
                                  Name = "J",
                                  Requires = new[] {"A"}
                              }).Should().BeFalse();

            items = collection.ToArray();
            items[0].Name.Should().Be("A");
            items[1].Name.Should().Be("C");
            items[2].Name.Should().Be("D");
            items[3].Name.Should().Be("E");

            collection.Contains(new Person {Name = "A"}).Should().BeTrue();
            collection.Contains(new Person {Name = "B"}).Should().BeFalse();
            collection.Contains(new Person {Name = "F"}).Should().BeFalse();
        }
    }
}