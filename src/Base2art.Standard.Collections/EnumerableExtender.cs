﻿namespace Base2art.Collections
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// A set of extension methods for <see cref="IEnumerable{T}"/>. 
    /// </summary>
    public static class EnumerableExtender
    {
        /// <summary>
        /// Splits an enumerable into chucks, by a maximum group size.
        /// </summary>
        /// <param name="source">The source to split</param>
        /// <param name="maxSize">The maximum number of items per group.</param>
        /// <typeparam name="T">The type of item to split</typeparam>
        /// <returns>A list of lists of the original items.</returns>
        public static IEnumerable<IEnumerable<T>> PartitionByMaxGroupSize<T>(this IEnumerable<T> source, int maxSize)
        {
            return new SplittingEnumerable<T>(source, maxSize);
        }
        
        /// <summary>
        /// Get A generic enumerator for an array.
        /// </summary>
        /// <param name="array">The item to get the enumerator for.</param>
        /// <typeparam name="T">The type of item to enumerate.</typeparam>
        /// <returns>The enumerator.</returns>
        public static IEnumerator<T> GetGenericEnumerator<T>(this T[] array)
        {
            return array.AsEnumerable().GetEnumerator();
        }

        /// <summary>
        /// Gets an array as readonly.
        /// </summary>
        /// <param name="array">The item to get as readonly.</param>
        /// <typeparam name="T">The type of item in the array.</typeparam>
        /// <returns>A readonly interface.</returns>
        public static IReadOnlyArrayList<T> AsReadOnlyArrayList<T>(this T[] array)
        {
            return new ReadOnlyArrayListWrapperForArray<T>(array);
        }

        /// <summary>
        /// Gets a list as readonly.
        /// </summary>
        /// <param name="array">The item to get as readonly.</param>
        /// <typeparam name="T">The type of item in the list.</typeparam>
        /// <returns>A readonly interface.</returns>
        public static IReadOnlyArrayList<T> AsReadOnlyArrayList<T>(this IList<T> array)
        {
            return new ReadOnlyArrayListWrapperForList<T>(array);
        }

        /// <summary>
        /// An method that executes an action for every item in an <see cref="IEnumerable{T}"/>. 
        /// </summary>
        /// <param name="collection">The collection to iterate over.</param>
        /// <param name="action">The action to execute.</param>
        /// <typeparam name="T">The type of item in the collection.</typeparam>
        public static void ForAll<T>(this IEnumerable<T> collection, Action<T> action)
        {
            action.ValidateIsNotNull();

            // ReSharper disable PossibleMultipleEnumeration
            collection.ValidateIsNotNull();

            foreach (var item in collection)
            {
                action(item);
            }

            // ReSharper restore PossibleMultipleEnumeration
        }

        /// <summary>
        /// Gets the index of an item in the list.
        /// </summary>
        /// <param name="collection">The item to scan.</param>
        /// <param name="value">The value to look for.</param>
        /// <typeparam name="T">The type of item in the collection.</typeparam>
        /// <returns>The index of the sought after item. </returns>
        public static int IndexOf<T>(this IEnumerable<T> collection, T value)
            where T : IComparable<T>
        {
            return collection.IndexOf(arg => value.CompareTo(arg) == 0);
        }

        /// <summary>
        /// Gets the index of an item in the list.
        /// </summary>
        /// <param name="collection">The item to scan.</param>
        /// <param name="func">The search function.</param>
        /// <typeparam name="T">The type of item in the collection.</typeparam>
        /// <returns>The index of the sought after item. </returns>
        public static int IndexOf<T>(this IEnumerable<T> collection, Func<T, bool> func)
        {
            func.ValidateIsNotNull();

            // ReSharper disable PossibleMultipleEnumeration
            collection.ValidateIsNotNull();

            var i = 0;
            foreach (var item in collection)
            {
                if (func(item))
                {
                    return i;
                }

                i++;
            }

            throw new KeyNotFoundException("No Item Found in the collection");

            // ReSharper restore PossibleMultipleEnumeration
        }

        /// <summary>
        /// A method that tells you if the item is in the collection.
        /// </summary>
        /// <param name="item">The item to search for.</param>
        /// <param name="collection">The collection to search.</param>
        /// <typeparam name="T">The type of item in the collection.</typeparam>
        /// <returns>A value indicating if the item is in the collection.</returns>
        public static bool In<T>(this T item, IEnumerable<T> collection)
            where T : IEquatable<T>
        {
            return InInternal(item, collection);
        }

        /// <summary>
        /// A method that tells you if the item is in the collection.
        /// </summary>
        /// <param name="item">The item to search for.</param>
        /// <param name="collection">The collection to search.</param>
        /// <typeparam name="T">The type of item in the collection.</typeparam>
        /// <returns>A value indicating if the item is in the collection.</returns>
        public static bool In<T>(this T item, params T[] collection)
            where T : IEquatable<T>
        {
            return InInternal(item, collection);
        }

        private static bool InInternal<T>(T item, IEnumerable<T> collection)
            where T : IEquatable<T>
        {
            // ReSharper disable PossibleMultipleEnumeration
            collection.ValidateIsNotNull();

            return collection.Any(item.Equals);
            // ReSharper restore PossibleMultipleEnumeration
        }
    }
}