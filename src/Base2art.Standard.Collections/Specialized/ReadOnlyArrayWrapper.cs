﻿namespace Base2art.Collections
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Justification = "SjY")]
    internal class ReadOnlyArrayListWrapperForArray<T> : IReadOnlyArrayList<T>
    {
        private readonly T[] values;

        public ReadOnlyArrayListWrapperForArray(T[] values)
        {
            this.values = values;
        }

        public int Count => this.values.Length;

        public T this[int index] => this.values[index];

        public IEnumerator<T> GetEnumerator()
        {
            return this.values.GetGenericEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}