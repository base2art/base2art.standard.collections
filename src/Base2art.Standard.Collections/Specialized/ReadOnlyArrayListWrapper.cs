﻿namespace Base2art.Collections
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Justification = "SjY")]
    internal class ReadOnlyArrayListWrapperForList<T> : IReadOnlyArrayList<T>
    {
        private readonly IList<T> values;

        public ReadOnlyArrayListWrapperForList(IList<T> values)
        {
            values.ValidateIsNotNull();

            this.values = values;
        }

        public int Count => this.values.Count;

        public T this[int index] => this.values[index];

        public IEnumerator<T> GetEnumerator()
        {
            return this.values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}