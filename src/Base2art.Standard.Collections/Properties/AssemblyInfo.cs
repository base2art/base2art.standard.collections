using System.Diagnostics.CodeAnalysis;
using System.Reflection;

[assembly: AssemblyTitle("Base2art.Standard.Collections")]
[assembly: AssemblyDescription("Set of classes, modules and libraries for easier interaction with collections.")]
[assembly: AssemblyConfiguration("")]

[assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace",
    Target = "Base2art.Collections", Justification = "SjY")]