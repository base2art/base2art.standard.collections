﻿namespace Base2art.Collections
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq.Expressions;

    /// <summary>
    /// A set of utility classes that generate ranges of data.
    /// </summary>
    public static class Range
    {
        /// <summary>
        /// Creates a range from 0 to the stop, stepping by 1.
        /// </summary>
        /// <param name="stop">The end of the range.</param>
        /// <typeparam name="T">The type of data.</typeparam>
        /// <returns>An enumerable of items.</returns>
        public static IEnumerable<T> Create<T>(T stop)
            where T : struct, IComparable<T>
        {
            return Create(Convert<T>(0), stop);
        }

        /// <summary>
        /// Creates a range from start to the stop, stepping by 1.
        /// </summary>
        /// <param name="start">The start of the range.</param>
        /// <param name="stop">The end of the range.</param>
        /// <typeparam name="T">The type of data.</typeparam>
        /// <returns>An enumerable of items.</returns>
        public static IEnumerable<T> Create<T>(T start, T stop)
            where T : struct, IComparable<T>
        {
            return Create(start, stop, Convert<T>(1));
        }

        /// <summary>
        /// Creates a range from start to the stop, stepping by the step.
        /// </summary>
        /// <param name="start">The start of the range.</param>
        /// <param name="stop">The end of the range.</param>
        /// <param name="step">The interval between each item.</param>
        /// <typeparam name="T">The type of data.</typeparam>
        /// <returns>An enumerable of items.</returns>
        public static IEnumerable<T> Create<T>(T start, T stop, T step)
            where T : struct, IComparable<T>
        {
            return CreateInternal(start, stop, step);
        }

        private static T Convert<T>(int i)
        {
            return (T) System.Convert.ChangeType(i, typeof(T), CultureInfo.InvariantCulture);
        }

        // http://stackoverflow.com/questions/8122611/c-sharp-adding-two-generic-values
        private static IEnumerable<T> CreateInternal<T>(T start, T stop, T step)
            where T : IComparable<T>
        {
            // Declare the parameters
            var paramA = Expression.Parameter(typeof(T), "x");
            var paramB = Expression.Parameter(typeof(T), "y");

            // Add the parameters together
            var body = Expression.Add(paramA, paramB);

            // Compile it
            var add = Expression.Lambda<Func<T, T, T>>(body, paramA, paramB).Compile();

            return new RangeCollection<T>(start, stop, step, add);
        }
    }
}