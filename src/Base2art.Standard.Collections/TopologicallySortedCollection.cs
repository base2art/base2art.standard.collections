﻿// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.Collections
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// A collection that sorts things topolocigally.
    /// Think of a directed graph, or a dependency tree when looking for example usages.
    /// </summary>
    /// <typeparam name="TKey">The objects Id.</typeparam>
    /// <typeparam name="TItem">the object inserted.</typeparam>
    public abstract class TopologicallySortedCollection<TKey, TItem>
        : ICollection<TItem>
        where TKey : IComparable<TKey>
        where TItem : class
    {
        private readonly LinkedList<Wrapper> items = new LinkedList<Wrapper>();

        /// <summary>Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.</summary>
        /// <returns>The number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.</returns>
        public int Count => this.items.Count;

        /// <summary>Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.</summary>
        /// <returns>true if the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only; otherwise, false.</returns>
        public bool IsReadOnly => false;

        /// <summary>Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.</summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.</exception>
        public void Add(TItem item)
        {
            var wrapper = new Wrapper(this, item);

            this.items.AddLast(wrapper);

            this.Refresh(wrapper);
            this.UpdateMain();
        }

        /// <summary>Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.</summary>
        /// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only. </exception>
        public void Clear() => this.items.Clear();

        /// <summary>Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.</summary>
        /// <returns>true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.</returns>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public bool Contains(TItem item)
        {
            return this.items.Any(x => x.Key.CompareTo(this.GetKey(item)) == 0);
        }

        /// <summary>Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.</summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.Generic.ICollection`1" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">The zero-based index in <paramref name="array" /> at which copying begins.</param>
        /// <exception cref="T:System.ArgumentNullException">
        /// <paramref name="array" /> is null.</exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        /// <paramref name="arrayIndex" /> is less than 0.</exception>
        /// <exception cref="T:System.ArgumentException">The number of elements in the source <see cref="T:System.Collections.Generic.ICollection`1" /> is greater than the available space from <paramref name="arrayIndex" /> to the end of the destination <paramref name="array" />.</exception>
        public void CopyTo(TItem[] array, int arrayIndex)
        {
            this.items.Select(x => x.Item).ToArray().CopyTo(array, arrayIndex);
        }

        /// <summary>Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.</summary>
        /// <returns>true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.</returns>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.</exception>
        public bool Remove(TItem item)
        {
            var key = this.GetKey(item);
            foreach (var other in this.items)
            {
                var inrefs = other.InRefs;
                for (var i = inrefs.Count - 1; i >= 0; i--)
                {
                    var inref = inrefs[i];
                    if (inref.Key.CompareTo(key) == 0)
                    {
                        inrefs.RemoveAt(i);
                    }
                }
            }

            var curr = this.items.First;
            while (curr != null)
            {
                if (curr.Value.Key.CompareTo(this.GetKey(item)) == 0)
                {
                    this.items.Remove(curr);
                    this.UpdateMain();
                    return true;
                }

                curr = curr.Next;
            }

            this.UpdateMain();
            return false;
        }

        /// <summary>Returns an enumerator that iterates through the collection.</summary>
        /// <returns>An enumerator that can be used to iterate through the collection.</returns>
        /// <filterpriority>1</filterpriority>
        public IEnumerator<TItem> GetEnumerator()
        {
            return this.items.Select(x => x.Item).GetEnumerator();
        }

        /// <summary>Returns an enumerator that iterates through a collection.</summary>
        /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
        /// <filterpriority>2</filterpriority>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// This method will order all the items in such a way as that you can process them linearlly.
        /// </summary>
        /// <returns>the resolved ordered dependenties</returns>
        public TItem[] Resolve()
        {
            return this.ToArray();
        }

        /// <summary>
        /// This method fetches the key (identity) for each item.
        /// </summary>
        /// <param name="item">The item whose key will be looked up.</param>
        /// <returns>The Key of the item.</returns>
        protected abstract TKey GetKey(TItem item);

        /// <summary>
        /// Gets the list of dependencies or returns an empty collection.
        /// </summary>
        /// <param name="item">The items for which the dependencies will be determined.</param>
        /// <returns>The list of dependencies.</returns>
        protected TKey[] GetDependenciesSafe(TItem item)
        {
            return (this.GetDependencies(item) ?? new TKey[0]).ToArray();
        }

        /// <summary>
        /// Gets the list of dependencies. Must be implemented by all inheritors.
        /// </summary>
        /// <param name="item">The items for which the dependencies will be determined.</param>
        /// <returns>The list of dependencies.</returns>
        protected abstract IEnumerable<TKey> GetDependencies(TItem item);

        private void UpdateMain()
        {
            var temp = new LinkedList<Wrapper>(this.items);
            this.items.Clear();
            foreach (var item in temp)
            {
                item.InRefsForMarking.Clear();
                item.InRefsForMarking.AddRange(item.InRefs);
            }

            this.AddToQueue(temp, this.items);
        }

        private void AddToQueue(LinkedList<Wrapper> input, LinkedList<Wrapper> output)
        {
            if (input.Count == 0)
            {
                return;
            }

            var first = input.FirstOrDefault(x => x.InRefsForMarking.Count == 0);
            if (first == null)
            {
                throw new InvalidOperationException("Circular reference (probably)");
            }

            input.Remove(first);
            output.AddFirst(first);
            foreach (var item in input)
            {
                if (item.InRefsForMarking.Contains(first))
                {
                    item.InRefsForMarking.Remove(first);
                }
            }

            this.AddToQueue(input, output);
        }

        private void Refresh(Wrapper wrapper)
        {
            foreach (var other in this.items)
            {
                if (Array.Exists(wrapper.Requires, x => x.CompareTo(other.Key) == 0))
                {
                    other.InRefs.Add(wrapper);
                }

                if (Array.Exists(other.Requires, x => x.CompareTo(wrapper.Key) == 0))
                {
                    wrapper.InRefs.Add(other);
                }
            }
        }

        private sealed class Wrapper
        {
            private readonly TopologicallySortedCollection<TKey, TItem> coll;

            public Wrapper(TopologicallySortedCollection<TKey, TItem> coll, TItem item)
            {
                this.Item = item;
                this.coll = coll;
                this.InRefs = new List<Wrapper>();
                this.InRefsForMarking = new List<Wrapper>();
            }

            public TKey Key => this.coll.GetKey(this.Item);

            public TKey[] Requires => this.coll.GetDependenciesSafe(this.Item);

            public TItem Item { get; }

            public List<Wrapper> InRefs { get; }

            public List<Wrapper> InRefsForMarking { get; }
        }
    }
}