﻿namespace Base2art.Collections
{
    using System;
    using System.Runtime.CompilerServices;

    internal static class Validation
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal static void ValidateIsNotNull<T>(this T value)
            where T : class
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
        }
    }
}