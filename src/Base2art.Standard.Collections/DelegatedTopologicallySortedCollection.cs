﻿// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.Collections
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// A collection that sorts things topolocigally, that is setup with delegates.
    /// Think of a directed graph, or a dependency tree when looking for example usages.
    /// </summary>
    /// <typeparam name="TKey">The objects Id.</typeparam>
    /// <typeparam name="TItem">the object inserted.</typeparam>
    public class DelegatedTopologicallySortedCollection<TKey, TItem> : TopologicallySortedCollection<TKey, TItem>
        where TKey : IComparable<TKey> where TItem : class
    {
        private readonly Func<TItem, TKey> keyFunc;
        private readonly Func<TItem, IEnumerable<TKey>> refsFunc;

        /// <summary>
        /// Initializes a new instance of the <see cref="DelegatedTopologicallySortedCollection{TKey,TItem}"/> class.
        /// </summary>
        /// <param name="keyFunc">The Function that looks up the key from the object.</param>
        /// <param name="refsFunc">The Function that looks up all the keys for items that are referenced by this object.</param>
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "SjY")]
        public DelegatedTopologicallySortedCollection(Func<TItem, TKey> keyFunc, Func<TItem, IEnumerable<TKey>> refsFunc)
        {
            keyFunc.ValidateIsNotNull();
            refsFunc.ValidateIsNotNull();

            this.keyFunc = keyFunc;
            this.refsFunc = refsFunc;
        }

        /// <summary>
        /// This method fetches the key (identity) for each item.
        /// </summary>
        /// <param name="item">The item whose key will be looked up.</param>
        /// <returns>The Key of the item.</returns>
        protected override TKey GetKey(TItem item)
        {
            return this.keyFunc(item);
        }

        /// <summary>
        /// Gets the list of dependencies. Must be implemented by all inheritors.
        /// </summary>
        /// <param name="item">The items for which the dependencies will be determined.</param>
        /// <returns>The list of dependencies.</returns>
        protected override IEnumerable<TKey> GetDependencies(TItem item)
        {
            return this.refsFunc(item);
        }
    }
}